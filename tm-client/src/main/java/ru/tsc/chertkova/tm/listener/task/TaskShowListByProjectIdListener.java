package ru.tsc.chertkova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.task.TaskShowListByProjectIdRequest;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractTaskListener;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskShowListByProjectIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-list-by-project-id";

    @NotNull
    public static final String DESCRIPTION = "Show task list by project id.";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskShowListByProjectIdListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final List<Task> tasks = getTaskEndpoint()
                .showListByProjectIdTask(new TaskShowListByProjectIdRequest(getToken(), projectId))
                .getTasks();
        renderTasks(tasks);
    }

}
