package ru.tsc.chertkova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.model.ICommand;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractListener;
import ru.tsc.chertkova.tm.listener.AbstractSystemListener;

import java.util.Collection;
import java.util.List;

@Component
public final class HelpListener extends AbstractSystemListener {

    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @NotNull
    public static final String NAME = "help";

    @NotNull
    public static final String DESCRIPTION = "Display list of terminal commands.";

    @NotNull
    public static final String ARGUMENT = "-h";

    @Override
    @EventListener(condition = "@helpListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[HELP]");
        for (@NotNull final AbstractListener listener : listeners) {
            System.out.println(listener.command() + " : "
                    + listener.description()
                    + ((listener.argument() != null) ? " : " + listener.argument() : "")
            );
        }
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String argument() {
        return ARGUMENT;
    }

}
