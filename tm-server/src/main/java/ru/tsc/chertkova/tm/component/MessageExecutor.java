package ru.tsc.chertkova.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.service.ISenderService;
import ru.tsc.chertkova.tm.model.EntityLog;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class MessageExecutor {

    @NotNull
    private static final int THREAD_COUNT = 3;

    @NotNull
    @Autowired
    private ISenderService service;

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@NotNull final Object object, @NotNull final String type) {
        es.submit(() -> {
            @NotNull final EntityLog entity = service.createMessage(object, type);
            service.send(entity);
        });
    }

    public void stop() {
        es.shutdown();
    }

}
