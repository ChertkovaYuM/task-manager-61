package ru.tsc.chertkova.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    boolean isEmailExists(@Nullable String email);

    @Nullable
    User setPassword(@Nullable String id,
                     @Nullable String password);

    @Nullable
    User updateUser(@Nullable String id,
                    @Nullable String firstName,
                    @Nullable String lastName,
                    @Nullable String middleName);

    User lockUserByLogin(@Nullable String login);

    User unlockUserByLogin(@Nullable String login);

    @Nullable
    User add(@Nullable User user);

    @Nullable
    User updateById(@Nullable String id,
                    @Nullable String firstName,
                    @Nullable String middleName,
                    @Nullable String lastName);

    boolean existsById(@Nullable String id);

    @Nullable
    User findById(@Nullable String id);

    User removeById(@Nullable String id);

    User remove(@Nullable User user);

    int getSize(@Nullable String id);

    void clear(@Nullable String id);

    @Nullable
    List<User> findAll(@Nullable String id);

    @Nullable
    List<User> addAll(@Nullable List<User> users);

    @Nullable
    List<User> removeAll(@Nullable List<User> users);

}
